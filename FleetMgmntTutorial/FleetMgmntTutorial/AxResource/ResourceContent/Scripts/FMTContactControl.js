// ---------------------------------------------------------------------
// <copyright file="FMTContactControl.js" company="Microsoft">
//      Copyright (c) Microsoft Corporation.  All rights reserved.
// </copyright>
// ---------------------------------------------------------------------

// As a best practice, wrap your code in function like this to protect the 'global namespace'.
// Also as a best practice, use strict to catch common JS errors.
(function () {
    'use strict';

    Globalize.addCultureInfo("en", {
        messages: {
            FMTContactControl_InvalidDate: "Invalid Date",
        },
    });

    $dyn.ui.defaults.FMTContactControl = {                
        Titles: [],
        Subtitles: [],
        Actions: [],

        // Data bound properties
        ImageField: '',
        TitleFields: [],
        SubtitleFields: [],
        ActionFields: [],
        DataSourceName: '',
    };

    $dyn.controls.FMTContactControl = function (props) {
        $dyn.telemetry.enabled = false;
        var self = this;

        // Almost all controls 'extend' the Control base class.  This syntax is used to call the contructor for the base class.
        $dyn.ui.Control.apply(this, arguments);

        $dyn.ui.applyDefaults(this, props, $dyn.ui.defaults.FMTContactControl);

        /* fieldToObservable allows for client side binding of fields. 
            Using a DataField and DataSource passed form the sever in a fieldBinding, it retrives the appropriate fieldObservable from the client side data source 
        */
        var fieldToObservable = function (fieldBinding) {
            var value = $dyn.observable();
            if ($dyn.value(self.DataSource)) {
                var model = $dyn.getModel($dyn.getModelName(fieldBinding.DataSource, self.DataSource), $dyn.value(self.DataSource));
                var field = fieldBinding.DataField ? fieldBinding.DataField : null;

                if (field) {
                    value = $dyn.getFieldObservable($dyn.getFieldName(field, model), $dyn.value(model));
                }
            }

            return value;
        }
        
        // Retrieves the field value for each binding present in TitleFields, joining each field
        self.Titles = $dyn.computed(
            function () {
                var valueArray = [];
                $.each($dyn.value(self.TitleFields), function (index, field) {
                    var fieldValue = $dyn.value(fieldToObservable(field));
                    fieldValue = $dyn.format(field.Format, fieldValue);
                    valueArray.push(fieldValue);
                });
                return valueArray.join(' ');
            });

        // Retrieves the field value for each binding present in SubtitleFields, running formatting logic on each field
        self.Subtitles = $dyn.computed(
            function () {
                var valueArray = [];
                $.each($dyn.value(self.SubtitleFields), function (index, field) {
                    var fieldValue = $dyn.value(fieldToObservable(field));
                    var number = new Number(fieldValue);
                    var date = new Date(fieldValue);
                    if (!isNaN(number)) {
                        fieldValue = number.toFixed(2);
                    }
                    else if (date != $dyn.label('FMTContactControl_InvalidDate')) {
                        fieldValue = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear();
                    }
                    fieldValue = $dyn.format(field.Format, fieldValue);
                    valueArray.push(fieldValue);
                });
                return valueArray;
            });

        self.Actions = $dyn.computed(
            function () {
                var valueArray = [];
                if ($.isArray($dyn.value(self.ActionFields))) {
                    $.each($dyn.value(self.ActionFields), function (index, action) {
                        valueArray.push({
                            Type: action.ActionName,
                            Value: $dyn.value(fieldToObservable(action))
                        });
                    });
                }
                return valueArray;
            });

        self.ActionClicked = function (action) {
            var type = $dyn.value(action.Type);
            if (!(self.ActionTypes[type] === undefined)) {
                // Look up the action from a limited set of known actions
                self.ActionTypes[type](action);
            }
            else {
                $dyn.callFunction(self.ExecuteAction, [type]);
            }
        };

        self.ActionTypes = {
            Email: function (action) {
                var url = 'mailto:' + $dyn.value(action.Value);
                self.Open(url);
            },
            Phone: function (action) {
                var number = $dyn.value(action.Value);
                number = number.replace(/\D/g, '');
                if (number.length == 10) {
                    // assume US country code
                    number = "1" + number;
                }
                var tel = "callto://+" + number;
                self.Open(tel);
            }
        };

        self.ActionSymbol = function (action) {
            return $dyn.value(action.Type) === 'Email' ? 'Mail' : $dyn.value(action.Type);
        }

        // Use of functions like window.open can be a problematic when unit testing so it is typical to create a function that can be mocked.
        self.Open = function (url) {
            var win = window.open(url);
            win.close();
        };
    };

    $dyn.controls.FMTContactControl.prototype = $dyn.extendPrototype($dyn.ui.Control.prototype, {
    });

})();


